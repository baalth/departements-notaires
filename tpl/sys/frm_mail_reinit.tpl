{# Formulaire oubli mot de passe #}

<div id="cadre_reinit">
  <form action="index.php?index=mail_reinit" method="post">
    <table>
      <tr>
        <td colspan="4"><p id="message_reinit">
          Merci d'indiquer votre identifiant de connexion (CRPCEN), afin de recevoir,
          sur l'adresse de messagerie associée à votre compte, un lien de réinitialisation de votre mot de passe.</p>
        </td>
      </tr>
      <tr>
        <td>
          <p id="message_reinit">Identifiant</p>
          <input type="text" id="user_login" name="user_login" required="required" onclick="viderLogin('user_login')">
        </td>
        <td><input type="submit" id="submit_reinit"></td>
      </tr>
    </table>
  </form>
</div>

<div id="message_r">
  {{message}}
</div>
