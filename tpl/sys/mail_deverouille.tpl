{# Corps mail déverrouillage du compte #}

<style>
.corps {
  font-family: 'verdana';
  font-size: 80%;
}
</style>
<p class="corps">
  Bonjour,
  <br />
  <br />
  <br /> Suite à {{config.max_login}} tentatives de connexion infructueuses, votre compte est verrouillé. 
  <br />
  <br />Le code de réactivation de votre compte est le suivant : <br/>
  <br />
  <b>{{unlock_key}}</b>
  <br />
  <br />Une fois votre compte déverrouillé, vous pourrez de nouveau vous connecter avec votre mot de passe actif ou
  choisir de le modifier via l'accueil.
  <br />
  <br />
  <br /> Cordialement, 
  <br />
  <b>{{config.nom_application}}</b>
</p>


