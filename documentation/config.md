# Configuration de l'application


___

## Généralités


Le fichier de configuration global se trouve à la racine : `config.php`.

Certaines adaptations se font dans des fichiers .php spécifiques qui sont signalés
au cas par cas.

Le dossier `pdf` mettre tous les autorisations 777 car il permet de générer les pdf et les images des stats.


<br/><br/>
___

## Configuration technique


### Variables générales
___

Mettre le nom de votre application :

```shell
$nom_application = "Départements et Notaires";
```


Mettre votre adresse du site :
```shell
$adresse_url_appli = "https://departements-notaires.xxxxx.fr";
```



### Connexion à la base de données
___

Adapter les variables suivantes :

```shell
$host_mysql = "localhost";
$user_mysql = "notaires";
$pass_mysql = "xxx";
$bdd_mysql  = "xxx";
```


### Envoi d'email
___

Adapter les variables suivantes :

```shell
$smtp_mail = "smtp.xxxx.fr";
$port_smtp_mail = "25";
$adresse_mail_notaire = "xxxxxx@xxxx.fr";
$password_mail = "xxxxxxxxxxx";
```

<br/><br/>

## Personnalisation graphique
___

### Logo application
___

```php
$logo_appli = "css/images/logo.png";
```

### favicon
___

```php
$site_favicon = "css/images/favicon.ico";
```

### Personnalisation des PDF
___

* Logo entête
La variable `$logo_pdf_departement` est un logo noir et blanc pour les courriers.
La variable `$logo_pdf_departement2` est un logo en couleur pour les statistiques.
```shell
$logo_pdf_departement = "css/images/logo_pdf.png";
$logo_pdf_departement2 = "css/images/logo_.png";
```

* Logo service
La variable `$logo_service_pdf` est le nom et/ou logo du service qui va traiter les courriers
```shell
$logo_service_pdf = "css/images/service_.png";
```

* Signature du chef du service
```shell
$signature_pdf = "css/images/signature.png";
```

* Bas page
```shell
$bas_page_pdf_adresse_department = "css/images/baspage_.png";
```

<br/><br/>

## CGU et Aide en ligne
___

### CGU
___

Le fichier CGU se trouve dans le dossier `tpl` :  `cgu.tpl`.

C'est dans ce fichier qu'on modifiera les conditions générales d'utilisations.

### Aide en ligne
___

Le fichier d'aide en ligne se trouve  dans le dossier `tpl` :  `aide.tpl`.

C'est dans ce fichier qu'on modifiera l'aide en ligne.

<br/><br/>

## Contenu des lettres et emails
___

Les fichiers qui se trouvent dans le dossier`tpl` permettent de personnaliser les différents fichiers pdf et email qui sont générés par l'application.

* `inconnu.tpl`
* `indus.tpl`
* `recup.tpl`
* `mail_reponse_ambigu.tpl`
* `mail_reponse_demande.tpl`
* `mail_reponse_inconnu.tpl`
* `mail_reponse_recup.tpl`


