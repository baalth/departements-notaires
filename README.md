![Logo](documentation/img/logo.jpg)

# Départements et Notaires

**Départements et Notaires** est un extranet permettant d'apporter une réponse en temps réel aux études notariales 
chargées d’une succession s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide sociale.

**Départements et Notaires** a été initialement conçu et développé par le Département du Rhône en lien avec la Chambre 
des Notaires du Rhône, et déployé la première fois en janvier 2016 sous le nom **Rhône + Notaires**.


## Fonctionnalités

* **Pour les notaires** :
  * Recherche et réponse immédiate, à l'écran et confirmation par email avec lettre jointe PDF
  * Historique de leurs propres recherches
* **Pour les gestionnaires** :
  * Historiques de toutes les recherches réalisées
  * Statistiques


## Installation et Documentation


* [Installation](documentation/installation.md)
* [Personnalisation de l'application](documentation/config.md)
* [Documentation fonctionnelle](documentation/fonctionnalites.md)


## Contacts

* Email : dev.notaires@rhone.fr
* [Site Web](https://www.rhone.fr/developpement_innovation/nouvelles_technologies/departements_notaires)

## Licence

[AGPL v3](LICENSE.txt) 
 

## Feuille de route

Bien qu'exploitée avec succès par le Département du Rhône depuis janvier 2016, l'application en version 1.3 n'était 
nullement destinée à être utilisée par une autre collectivité. 
La maintenabilité du code ou le respect des standards n'étaient donc pas des critères prioritaires par rapport au coût 
de réalisation et au gain de productivité offert en final.

Dans le cadre de la démarche d'ouverture entreprise avec l'Adullact, nous souhaitions pouvoir diffuser le plus rapidement
le code, avec les adaptations **minimales** facilitant la personnalisation. Cet objectif est visé par la version **1.4**.

Nous envisageons dans une seconde grande étape de porter l'application sous Symfony 3, afin d'avoir un socle robuste pour
apporter les évolutions futures. Cet objectif est visé par la version **1.5**.


Liste des versions :
* **Version actuelle** : 
  * **1.4** : Version en cours de packaging pour diffusion via la forge de l'Adullact. Le code, écrit initialement
  en "plain PHP" est modifié à minima pour permettre une personnalisation de l'application, la documentation associée
  est créée pour faciliter l'installation et la prise en main par d'autres collectivités.
  
* **Versions à venir** :
  * **1.5** : Version utilisant le framework Symfony 3
  
